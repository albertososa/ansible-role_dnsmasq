# Ansible role: dnsmasq

Deploys dnsmasq.

## Requirements

None.

## Role variables

Available variables are listed below, along with the default values (see `defaults/main.yml`):

```yaml
dnsmasq_listen_interface: eth0
```

The name of the interface that dnsmasq listen to.

```yaml
dnsmasq_listen_address: 192.168.1.1
```

The IP address that dnsmasq listen to.


```yaml
dnsmasq_dns_servers: []
```

Set a list with upstream DNS servers. For example:

```yaml
dnsmasq_dns_servers:
  - 8.8.8.8
  - 8.8.4.4
```

```yaml
dnsmasq_host_records: []
```

Set a list with DNS host records. For example:

```yaml
dnsmasq_host_records:
  - hostname: vm-example-01
    ip_address: 192.168.1.1
```

## Author

Alberto Sosa (info@albertososa.es)
